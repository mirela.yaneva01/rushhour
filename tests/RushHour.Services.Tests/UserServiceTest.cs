﻿using Moq;
using RushHour.Domain.Abstractions.Helpers;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Constants;
using RushHour.Domain.Dtos;
using RushHour.Domain.Enums;
using RushHour.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Services.Tests
{
    public class UserServiceTest
    {
        public Mock<IUserRepository> userRepositoryMock = new Mock<IUserRepository>();
        public Mock<IHasherService> hasherServiceMock = new Mock<IHasherService>();
        public Mock<IJwtService> jwtServiceMock = new Mock<IJwtService>();

        private UserService userService;
        private List<UserDto> userDtos;
        private UserDto userDto;
        private PasswordAndSaltDto passwordAndSaltDto;

        public UserServiceTest()
        {
            userService = new UserService(userRepositoryMock.Object, hasherServiceMock.Object, jwtServiceMock.Object);
            userDtos = new List<UserDto>
            {
                new UserDto
                {
                    Email = "Vanko0309"
                }
            };
            userDto = new UserDto { 
                Email = "Vanko0309",
                Role = Role.Admin,
                Id = Guid.NewGuid(),
                Salt = "peper (oni)",
                Password = "Miki, no hashnata"
            };
            passwordAndSaltDto = new PasswordAndSaltDto { Password = "123456G", Salt = "6789I" };
        }

        [Fact]
        public async Task Create_NoErrorsOrIssues_Expect_CreatedInDatabase()
        {
            //Arrange
            userRepositoryMock.Setup(x => x.GetByEmailAsync(userDto.Email));
            hasherServiceMock.Setup(x => x.HashPassword(userDto.Password))
                .Returns(passwordAndSaltDto);
            userRepositoryMock.Setup(x => x.CreateAsync(userDto));
            
            //Act
            await userService.CreateAsync(userDto);

            //Assert
            userRepositoryMock.Verify(x => x.GetByEmailAsync(It.IsAny<string>()));
            userRepositoryMock.Verify(x => x.CreateAsync(It.Is<UserDto>(x =>
                x.Role == Role.User &&
                x.Password == passwordAndSaltDto.Password &&
                x.Salt == passwordAndSaltDto.Salt &&
                x.Email == userDto.Email.ToLower())), Times.Once());
            hasherServiceMock.Verify(x => x.HashPassword(It.IsAny<string>()), Times.Once());
        }

        [Fact]
        public async Task Create_EmailExist_Expect_Exception()
        {
            //Arrange
            userRepositoryMock.Setup(x => x.GetByEmailAsync(userDto.Email))
                .Returns(Task.FromResult(new UserDto()));

            //Act/Assert
            var ex = await Assert.ThrowsAsync<AppException>(() => userService.CreateAsync(userDto));

            Assert.Equal(StringConstants.UserExists, ex.Message);
        }

        [Fact]
        public async Task GetAll_NoErrorsOrIssues_Expect_ListOfUserDtos()
        {
            //Arrange
            var offset = 2;
            var limit = 3;
            userDtos.Add(userDto);

            userRepositoryMock.Setup(x => x.GetAllAsync(offset, limit, null))
                .Returns(Task.FromResult(userDtos));

            //Act
            var result = await userService.GetAllAsync(offset, limit);

            //Assert
            userRepositoryMock.Verify(x => x.GetAllAsync(offset, limit, null), Times.Once());

            Assert.Equal(userDtos.Count, result.Count);
            Assert.Equal(userDtos[0].Email.ToLower(), result[0].Email.ToLower());

        }

        [Fact]
        public async Task GetById_NoErrorsOrIssues_Expect_UserDto()
        {
            //Arrange
            var id = Guid.NewGuid();

            userRepositoryMock.Setup(x => x.GetByIdAsync(id))
                .Returns(Task.FromResult(userDto));

            //Act
            var result = await userService.GetByIdAsync(id);

            //Assert
            userRepositoryMock.Verify(x => x.GetByIdAsync(id), Times.Once());

            Assert.Equal(userDto.Email.ToLower(), result.Email.ToLower());
        }

        [Fact]
        public async Task Update_NoErrorsOrIssues_Expect_RecordUpdatedInDatabase()
        {
            //Arrange
            userRepositoryMock.Setup(x => x.UpdateAsync(userDto));

            //Act
            await userService.UpdateAsync(userDto);

            //Assert
            userRepositoryMock.Verify(x => x.UpdateAsync(userDto), Times.Once());
        }

        [Fact]
        public async Task Delete_NoErrorsOrIssues_Expect_RecordDeletedFromDatabase()
        {
            //Arrange
            var id = Guid.NewGuid();

            userRepositoryMock.Setup(x => x.DeleteAsync(id));

            //Act
            await userService.DeleteAsync(id);

            //Assert
            userRepositoryMock.Verify(x => x.DeleteAsync(id));
        }

        [Fact]
        public async Task Login_NoErrorsOrIssue_Expect_ReturnToken()
        {
            //Arrange
            var email = "Miki0203";
            var password = "123456";
            var token = "4564213";
            userDto.Email = email;

            userRepositoryMock.Setup(x => x.GetByEmailAsync(email))
                .Returns(Task.FromResult(userDto));
            hasherServiceMock.Setup(x => x.VerifyHashedPassword(password, userDto.Password, userDto.Salt))
                .Returns(true);
            jwtServiceMock.Setup(x => x.GenerateJsonWebToken(userDto.Role, userDto.Id))
                .Returns(token);

            //Act
            var result = await userService.LoginAsync(email, password);

            //Assert
            userRepositoryMock.Verify(x => x.GetByEmailAsync(It.Is<string>(x => x == email)), Times.Once());
            hasherServiceMock.Verify(x => x.VerifyHashedPassword(It.Is<string>(x => x == password),
                It.Is<string>(x => x == userDto.Password),
                It.Is<string>(x => x == userDto.Salt)), Times.Once());
            jwtServiceMock.Verify(x => x.GenerateJsonWebToken(It.Is<Role>(x => x == userDto.Role),
                It.Is<Guid>(x => x == userDto.Id)), Times.Once());

            Assert.Equal(token, result);
        }

        [Fact]
        public async Task Login_UserExist_Expect_Exception()
        {
            //Arrange
            userRepositoryMock.Setup(x => x.GetByEmailAsync(It.IsAny<string>()));

            //Act/Assert
            var ex = await Assert.ThrowsAsync<AppException>(() => userService.LoginAsync(It.IsAny<string>(), It.IsAny<string>()));

            userRepositoryMock.Verify(x => x.GetByEmailAsync(It.IsAny<string>()), Times.Once());
            hasherServiceMock.Verify(x => x.VerifyHashedPassword(It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>()), Times.Never());
            jwtServiceMock.Verify(x => x.GenerateJsonWebToken(It.IsAny<Role>(),
                It.IsAny<Guid>()), Times.Never());

            Assert.Equal(StringConstants.InvalidUserOrPass, ex.Message);
        }

        [Fact]
        public async Task Login_VerifyHashFalse_Expect_Exception()
        {
            //Arrange

            userRepositoryMock.Setup(x => x.GetByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(userDto));
            hasherServiceMock.Setup(x => x.VerifyHashedPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(false);

            //Act/Assert
            var ex = await Assert.ThrowsAsync<AppException>(() => userService.LoginAsync(It.IsAny<string>(), It.IsAny<string>()));

            userRepositoryMock.Verify(x => x.GetByEmailAsync(It.IsAny<string>()), Times.Once());
            hasherServiceMock.Verify(x => x.VerifyHashedPassword(It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>()), Times.Once());
            jwtServiceMock.Verify(x => x.GenerateJsonWebToken(It.IsAny<Role>(),
                It.IsAny<Guid>()), Times.Never());

            Assert.Equal(StringConstants.InvalidUserOrPass, ex.Message);
        }

    }
}
