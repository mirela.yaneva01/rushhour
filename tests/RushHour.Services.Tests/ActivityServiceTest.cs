﻿using Moq;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Services.Tests
{
    public class ActivityServiceTest
    {
        public Mock<IActivityRepository> activityRepositoryMock = new Mock<IActivityRepository>();

        private ActivityService activityService;
        private List<ActivityDto> activityDtos;
        private ActivityDto activityDto;

        public ActivityServiceTest()
        {
            activityService = new ActivityService(activityRepositoryMock.Object);
            activityDtos = new List<ActivityDto>
            {
                new ActivityDto
                {
                    Name = "Miki"
                }
            };
            activityDto = new ActivityDto { Name = "Miki" };
        }

        [Fact]
        public async Task Create_NoErrorsOrIssues_Expect_CreatedInDatabase()
        {
            //Arrange 
            var activityDto = new ActivityDto();
            activityRepositoryMock.Setup(x => x.CreateAsync(activityDto));

            //Act
            await activityService.CreateAsync(activityDto);

            //Assert
            activityRepositoryMock.Verify(x => x.CreateAsync(activityDto), Times.Once());
        }

        [Fact]
        public async Task GetAll_NoErrorsOrIssues_Expect_ListOfActivityDtos()
        {
            //Arrange
            var offset = 1;
            var limit = 2;
           
            activityRepositoryMock.Setup(x => x.GetAllAsync(offset, limit, null))
                .Returns(Task.FromResult(activityDtos));

            //Act
            var result = await activityService.GetAllAsync(offset, limit);

            //Assert
            activityRepositoryMock.Verify(x => x.GetAllAsync(offset, limit, null), Times.Once());

            Assert.Equal(activityDtos.Count, result.Count);
            Assert.Equal(activityDtos[0].Name, result[0].Name);
        }

        [Fact]
        public async Task GetById_NoErrorsOrIssues_Expect_ActivityDto()
        {
            //Arrange
            var id = Guid.NewGuid();

            activityRepositoryMock.Setup(x => x.GetByIdAsync(id))
                .Returns(Task.FromResult(activityDto));

            //Act
            var result = await activityService.GetByIdAsync(id);

            //Assert
            activityRepositoryMock.Verify(x => x.GetByIdAsync(id), Times.Once());

            Assert.Equal(activityDto.Name, result.Name);
        }

        [Fact]
        public async Task Update_NoErrorsOrIssues_Expect_RecordUpdatedInDatabase()
        {
            //Arrange
            activityRepositoryMock.Setup(x => x.UpdateAsync(activityDto));

            //Act
            await activityService.UpdateAsync(activityDto);

            //Assert
            activityRepositoryMock.Verify(x => x.UpdateAsync(activityDto), Times.Once());
        }

        [Fact]
        public async Task Delete_NoErrorsOrIssues_Expect_RecordDeletedFromDatabase()
        {
            //Arrange
            var id = Guid.NewGuid();

            activityRepositoryMock.Setup(x => x.DeleteAsync(id));

            //Act
            await activityService.DeleteAsync(id);

            //Assert
            activityRepositoryMock.Verify(x => x.DeleteAsync(id), Times.Once());
        }
    }
}
