﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Moq;
using RushHour.Domain;
using RushHour.Domain.Abstractions.Helpers;
using RushHour.Domain.Enums;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Services.Tests
{
    public class JwtServiceTest
    {
        public Mock<IDateTimeProvider> dateTimeProviderMock = new Mock<IDateTimeProvider>();

        private JwtService jwtService;
        private JwtSettings jwtSettings;

        public JwtServiceTest()
        {
            var options = Options.Create(new JwtSettings
            {
                Audience = "Miki",
                Issuer = "Piki",
                ExpirationInMinutes = 5,
                Key = "eyJhbGciOiJIUzI1NiJ8.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTYyNzkxMDkwNywiaWF0IjoxNjI3OTEwOTA3fQ.BAxCh_zy3Vok24o6WKzlAfReXUbGPIHTG05D3shywtU"
            });
            jwtSettings = options.Value;

            jwtService = new JwtService(options, dateTimeProviderMock.Object);
        }

        [Fact]
        public void GenerateJsonWebToken_NoErrorOrIssues_Expect_Token()
        {
            //Arrange
            var role = Role.User;
            var userId = Guid.NewGuid();
            var dateTime = DateTime.UtcNow;

            dateTimeProviderMock.Setup(x => x.GetNow())
                .Returns(dateTime);

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Role, role.ToString()));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, userId.ToString()));

            var token = new JwtSecurityToken(claims: claims,
                audience: jwtSettings.Audience,
                issuer: jwtSettings.Issuer,
                expires: dateTime.Add(TimeSpan.FromMinutes(jwtSettings.ExpirationInMinutes)),
                signingCredentials: credentials);

            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            //Act 
            var result = jwtService.GenerateJsonWebToken(role, userId);

            //Assert
            dateTimeProviderMock.Verify(x => x.GetNow(), Times.Once());

            Assert.Equal(tokenString, result);
        }
    }
}
