﻿using Moq;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Constants;
using RushHour.Domain.Dtos;
using RushHour.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Services.Tests
{
    public class AppointmentServiceTest
    {
        public Mock<IAppointmentRepository> appointmentRepositoryMock = new Mock<IAppointmentRepository>();
        public Mock<IActivityRepository> activityRepositoryMock = new Mock<IActivityRepository>();
        public Mock<IAppointmentActivityRepository> appointmentActivityRepositoryMock = new Mock<IAppointmentActivityRepository>();

        private AppointmentService appointmentService;
        private List<AppointmentDto> appointmentDtos;
        private AppointmentDto appointment;
        private AppointmentDto dbAppointment;
        private ActivityDto activity;
        private ActivityDto activity2;
        private List<AppointmentActivityDto> appointmentActivities;
        private AppointmentActivityDto appointmentActivity;
        private AppointmentActivityDto appointmentActivity2;
        

        public AppointmentServiceTest()
        {
            appointmentService = new AppointmentService(appointmentRepositoryMock.Object, 
                activityRepositoryMock.Object,
                appointmentActivityRepositoryMock.Object);
            appointmentDtos = new List<AppointmentDto>
            {
                new AppointmentDto 
                {
                    UserId = Guid.NewGuid()
                }
            };
            appointment = new AppointmentDto { UserId = Guid.NewGuid(), Id = Guid.NewGuid() };
            dbAppointment = new AppointmentDto {UserId = Guid.NewGuid() };
            activity = new ActivityDto { Id = Guid.NewGuid() };
            activity2 = new ActivityDto { Id = Guid.NewGuid() };
            appointmentActivities = new List<AppointmentActivityDto>();
            activity = new ActivityDto { Id = Guid.NewGuid() };
            activity2 = new ActivityDto { Id = Guid.NewGuid() };
            appointmentActivity = new AppointmentActivityDto { ActivityId = Guid.NewGuid() };
            appointmentActivity2 = new AppointmentActivityDto { ActivityId = Guid.NewGuid() };
        }

        [Fact]
        public async Task Create_NoErrorsOrIssues_Expect_CreatedInDatabase()
        {
            //Arrange
            appointmentRepositoryMock.Setup(x => x.GetAllAsync(null, null, It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
                .Returns(Task.FromResult(appointmentDtos));
            appointmentRepositoryMock.Setup(x => x.CreateAsync(appointment));

            //Act
            await appointmentService.CreateAsync(appointment, appointment.UserId);

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetAllAsync(It.Is<int?>(x => x == null),
                It.Is<int?>(x => x == null), It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());
            appointmentRepositoryMock.Verify(x => x.CreateAsync(appointment), Times.Once());
        }


        [Fact]
        public async Task Create_OverLappingAppointment1_Expect_Exeption()
        {
            //Arrange
            appointmentDtos.Add(appointment);
            appointmentDtos.Add(dbAppointment);
            dbAppointment.StartDate = DateTime.UtcNow.Date;
            dbAppointment.EndDate = dbAppointment.StartDate.AddDays(4);
            appointment.StartDate = dbAppointment.StartDate.AddDays(1);

            appointmentRepositoryMock.Setup(x => x.GetAllAsync(null, null, It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
                .Returns(Task.FromResult(appointmentDtos));
            appointmentRepositoryMock.Setup(x => x.CreateAsync(dbAppointment));

            //Act
            var ex = await Assert.ThrowsAsync<AppException>(() => appointmentService.CreateAsync(appointment, appointment.UserId));

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetAllAsync(It.Is<int?>(x => x == null),
                It.Is<int?>(x => x == null), It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());
            appointmentRepositoryMock.Verify(x => x.CreateAsync(It.IsAny<AppointmentDto>()), Times.Never());

            Assert.Equal(StringConstants.InvalidAppointment, ex.Message);
        }

        [Fact]
        public async Task Create_OverLappingAppointment2_Expect_Exeption()
        {
            //Arrange
            appointmentDtos.Add(appointment);
            appointmentDtos.Add(dbAppointment);
            dbAppointment.StartDate = DateTime.UtcNow.Date;
            dbAppointment.EndDate = dbAppointment.StartDate.AddDays(4);
            appointment.EndDate = dbAppointment.EndDate.AddDays(-1);

            appointmentRepositoryMock.Setup(x => x.GetAllAsync(null, null, It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
                .Returns(Task.FromResult(appointmentDtos));
            appointmentRepositoryMock.Setup(x => x.CreateAsync(dbAppointment));

            //Act
            var ex = await Assert.ThrowsAsync<AppException>(() => appointmentService.CreateAsync(appointment, appointment.UserId));

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetAllAsync(It.Is<int?>(x => x == null),
                It.Is<int?>(x => x == null), It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());
            appointmentRepositoryMock.Verify(x => x.CreateAsync(It.IsAny<AppointmentDto>()), Times.Never());

            Assert.Equal(StringConstants.InvalidAppointment, ex.Message);
        }

        [Fact]
        public async Task Create_OverLappingAppointment3_Expect_Exeption()
        {
            //Arrange
            appointmentDtos.Add(appointment);
            appointmentDtos.Add(dbAppointment);
            dbAppointment.StartDate = DateTime.UtcNow.Date;
            dbAppointment.EndDate = dbAppointment.StartDate.AddDays(4);
            appointment.EndDate = dbAppointment.EndDate.AddDays(2);
            appointment.StartDate = dbAppointment.StartDate.AddDays(-2);

            appointmentRepositoryMock.Setup(x => x.GetAllAsync(null, null, It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
                .Returns(Task.FromResult(appointmentDtos));
            appointmentRepositoryMock.Setup(x => x.CreateAsync(dbAppointment));

            //Act
            var ex = await Assert.ThrowsAsync<AppException>(() => appointmentService.CreateAsync(appointment, appointment.UserId));

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetAllAsync(It.Is<int?>(x => x == null),
                It.Is<int?>(x => x == null), It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());
            appointmentRepositoryMock.Verify(x => x.CreateAsync(It.IsAny<AppointmentDto>()), Times.Never());

            Assert.Equal(StringConstants.InvalidAppointment, ex.Message);
        }

        [Fact]
        public async Task GetAll_NoErrorsOrIssues_Expect_ListOfAppointmentDtos()
        {
            //Arrange
            var offset = 1;
            var limit = 2;
            var userId = Guid.NewGuid();

            appointmentRepositoryMock.Setup(x => x.GetAllAsync(offset, limit, x => x.UserId == userId))
                .Returns(Task.FromResult(appointmentDtos));

            //Act
            var result = await appointmentService.GetAllAsync(offset, limit, userId);

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetAllAsync(It.Is<int?>(x => x == offset),
             It.Is<int?>(x => x == limit),
             It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());

            Assert.Equal(appointmentDtos.Count, result.Count);
            Assert.Equal(appointmentDtos[0].UserId, result[0].UserId);
        }

        [Fact]
        public async Task GetById_NoErrorsOrIssues_Expect_AppointmentDto()
        {
            //Arrange
            appointmentRepositoryMock.Setup(x => x.GetByIdAsync(appointment.Id))
                .Returns(Task.FromResult(appointment));

            //Act
            var result = await appointmentService.GetByIdAsync(appointment.Id, appointment.UserId);

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetByIdAsync(It.Is<Guid>(x => x == appointment.Id)), Times.Once());

            Assert.Equal(appointment, result);
        }

        [Fact]
        public async Task GetById_UserNotOwner_Expect_Null()
        {
            //Arrange
            var userId = Guid.NewGuid();

            appointmentRepositoryMock.Setup(x => x.GetByIdAsync(appointment.Id))
                .Returns(Task.FromResult(appointment));

            //Act
            var result = await appointmentService.GetByIdAsync(appointment.Id, userId);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task Update_NoErrorsOrIssues_Expect_RecordUpdatedInDatabase()
        {
            //Arrange
            appointmentRepositoryMock.Setup(x => x.UpdateAsync(appointment));
            appointmentRepositoryMock.Setup(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
                .Returns(Task.FromResult(true));

            //Act
           await appointmentService.UpdateAsync(appointment, appointment.UserId);

            //Assert
            appointmentRepositoryMock.Verify(x => x.UpdateAsync(
                It.Is<AppointmentDto>(x => x.UserId == appointment.UserId)), Times.Once());
            appointmentRepositoryMock.Verify(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());
        }

        [Fact]
        public async Task Update_UserNotOwner_Expect_NoUpdate()
        {
            //Arrange
            var userId = Guid.NewGuid();

            appointmentRepositoryMock.Setup(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
                .Returns(Task.FromResult(false));

            //Act
            await appointmentService.UpdateAsync(appointment, userId);

            //Assert
            appointmentRepositoryMock.Verify(x => x.UpdateAsync(
                It.IsAny<AppointmentDto>()), Times.Never());
        }

        [Fact]
        public async Task Delete_NoErrorsOrIssues_Expect_RecordDeletedFromDatabase()
        {
            //Arrange
            var appointmentId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            appointmentRepositoryMock.Setup(x => x.DeleteAsync(appointmentId));
            appointmentRepositoryMock.Setup(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
                .Returns(Task.FromResult(true));

            //Act
           await appointmentService.DeleteAsync(appointmentId, userId);

            //Assert
            appointmentRepositoryMock.Verify(x => x.DeleteAsync(appointmentId), Times.Once());
            appointmentRepositoryMock.Verify(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());
        }

        [Fact]
        public async Task Delete_UserNotOwner_Expect_NoDelete()
        {
            //Arrange
            appointmentRepositoryMock.Setup(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
                .Returns(Task.FromResult(false));

            //Act
            await appointmentService.DeleteAsync(Guid.NewGuid(), Guid.NewGuid());

            //Assert
            appointmentRepositoryMock.Verify(x => x.DeleteAsync(It.IsAny<Guid>()), Times.Never());
        }

        [Fact]
        public async Task AddActivityToAppointment_NoErrorsOrIssues_Expect_ActivityAddedToAppointment()
        {
            //Arrange
            var appointment = new AppointmentDto();
            var activity = new ActivityDto();
            var appointmentLength = 15.6m;
            appointment.Id = Guid.NewGuid();
            appointment.UserId = Guid.NewGuid();
            activity.Id = Guid.NewGuid();
            appointment.StartDate = DateTime.UtcNow;
            appointment.EndDate = appointment.StartDate.AddDays(5);

            appointmentRepositoryMock.Setup(x => x.GetByIdAsync(appointment.Id))
                .Returns(Task.FromResult(appointment));
            activityRepositoryMock.Setup(x => x.GetByIdAsync(activity.Id))
                .Returns(Task.FromResult(activity));
            appointmentActivityRepositoryMock.Setup(x => x.SumForDurationAppointmentAsync(appointment.Id))
                .Returns(Task.FromResult(appointmentLength));
            appointmentActivityRepositoryMock.Setup(x => x.CreateAsync(It.Is<AppointmentActivityDto>(x =>
            x.ActivityId == activity.Id &&
            x.AppointmentId == appointment.Id)));

            //Act
            await appointmentService.AddActivityToAppointmentAsync(appointment.Id, appointment.UserId, activity.Id);

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetByIdAsync(It.Is<Guid>(x => x == appointment.Id)), Times.Once());
            activityRepositoryMock.Verify(x => x.GetByIdAsync(It.Is<Guid>(x => x == activity.Id)), Times.Once());
            appointmentActivityRepositoryMock.Verify(x => x.SumForDurationAppointmentAsync(It.Is<Guid>(x => x == appointment.Id)), Times.Once());
            appointmentActivityRepositoryMock.Verify(x => x.CreateAsync(It.Is<AppointmentActivityDto>(x =>
            x.ActivityId == activity.Id &&
            x.AppointmentId == appointment.Id)), Times.Once());
        }

        [Fact]
        public async Task AddActivityToAppointment_UserNotOwner_Expect_NotAdded()
        {
            //Arrange
            var appointment = new AppointmentDto();
            appointment.UserId = Guid.NewGuid();
            var userId = Guid.NewGuid();

            appointmentRepositoryMock.Setup(x => x.GetByIdAsync(appointment.Id))
                .Returns(Task.FromResult(appointment));
           
            //Act
            await appointmentService.AddActivityToAppointmentAsync(appointment.Id, userId, Guid.NewGuid());

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetByIdAsync(It.Is<Guid>(x => x == appointment.Id)), Times.Once());
            activityRepositoryMock.Verify(x => x.GetByIdAsync(It.IsAny<Guid>()), Times.Never());
            appointmentActivityRepositoryMock.Verify(x => x.SumForDurationAppointmentAsync(It.IsAny<Guid>()), Times.Never());
            appointmentActivityRepositoryMock.Verify(x => x.CreateAsync(It.IsAny<AppointmentActivityDto>()), Times.Never());
        }

        [Fact]
        public async Task AddActivityToAppointment_ToLong_Expect_Exeption()
        {
            //Arrange
            var appointmentLength = 60m;
            appointment.StartDate = DateTime.UtcNow;
            appointment.EndDate = appointment.StartDate.AddHours(1);
            activity.DurationMinutes = 10;

            appointmentRepositoryMock.Setup(x => x.GetByIdAsync(appointment.Id))
                .Returns(Task.FromResult(appointment));
            activityRepositoryMock.Setup(x => x.GetByIdAsync(activity.Id))
                .Returns(Task.FromResult(activity));
            appointmentActivityRepositoryMock.Setup(x => x.SumForDurationAppointmentAsync(appointment.Id))
                .Returns(Task.FromResult(appointmentLength));

            //Act
            var ex = await Assert.ThrowsAsync<AppException>(() => 
                appointmentService.AddActivityToAppointmentAsync(appointment.Id, appointment.UserId, activity.Id));

            //Assert
            appointmentRepositoryMock.Verify(x => x.GetByIdAsync(It.Is<Guid>(x => x == appointment.Id)), Times.Once());
            activityRepositoryMock.Verify(x => x.GetByIdAsync(It.Is<Guid>(x => x == activity.Id)), Times.Once());
            appointmentActivityRepositoryMock.Verify(x => x.SumForDurationAppointmentAsync(It.Is<Guid>(x => x == appointment.Id)), Times.Once());
            appointmentActivityRepositoryMock.Verify(x => x.CreateAsync(It.IsAny<AppointmentActivityDto>()), Times.Never());

            Assert.Equal(StringConstants.InvalidActivity, ex.Message);
        }

        [Fact]
        public async Task GetActivitiesForAppointment_NoErrorsOrIssues_Expect_ListOfActivities()
        {
            //Arrange
            var id = Guid.NewGuid();
            var userId = Guid.NewGuid();

            activity.Id = appointmentActivity.ActivityId;
            appointmentActivities.Add(appointmentActivity);

            activity2.Id = appointmentActivity2.ActivityId;
            appointmentActivities.Add(appointmentActivity2);

            appointmentRepositoryMock.Setup(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
               .Returns(Task.FromResult(true));
            appointmentActivityRepositoryMock.Setup(x => x.GetAllActivitiesForAppointmentAsync(id))
                .Returns(Task.FromResult(appointmentActivities));
            activityRepositoryMock.Setup(x => x.GetByIdAsync(appointmentActivity.ActivityId))
                .Returns(Task.FromResult(activity));
            activityRepositoryMock.Setup(x => x.GetByIdAsync(appointmentActivity2.ActivityId))
                .Returns(Task.FromResult(activity2));

            //Act
            var result = await appointmentService.GetActivitiesForAppointmentAsync(id, userId);

            //Arrange
            appointmentRepositoryMock.Verify(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());
            appointmentActivityRepositoryMock.Verify(x => x.GetAllActivitiesForAppointmentAsync(It.Is<Guid>(x => x == id)), Times.Once());
            activityRepositoryMock.Verify(x => x.GetByIdAsync(It.Is<Guid>(x => x == appointmentActivity.ActivityId)), Times.Once());
            activityRepositoryMock.Verify(x => x.GetByIdAsync(It.Is<Guid>(x => x == appointmentActivity2.ActivityId)), Times.Once());
            activityRepositoryMock.Verify(x => x.GetByIdAsync(It.IsAny<Guid>()), Times.Exactly(appointmentActivities.Count));

            Assert.Equal(appointmentActivities.Count, result.Count);
            Assert.Equal(appointmentActivities[0].ActivityId, result[0].Id);
            Assert.Equal(appointmentActivities[1].ActivityId, result[1].Id);
        }

        [Fact]
        public async Task GetActivitiesForAppointment_NoAppointment_Expect_Null()
        {
            //Arrange
            appointmentRepositoryMock.Setup(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()))
               .Returns(Task.FromResult(false));
            
            //Act
            var result = await appointmentService.GetActivitiesForAppointmentAsync(It.IsAny<Guid>(), It.IsAny<Guid>());

            //Arrange
            appointmentRepositoryMock.Verify(x => x.AnyAsync(It.IsAny<Expression<Func<AppointmentDto, bool>>>()), Times.Once());
            appointmentActivityRepositoryMock.Verify(x => x.GetAllActivitiesForAppointmentAsync(It.IsAny<Guid>()), Times.Never());
            activityRepositoryMock.Verify(x => x.GetByIdAsync(It.IsAny<Guid>()), Times.Never);

            Assert.Null(result);
        }
    }
}
