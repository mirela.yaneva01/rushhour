﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Moq;
using RushHour.Domain.Abstractions.Helpers;
using RushHour.Domain.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RushHour.Services.Tests
{
    public class HasherServiceTest
    {
        public Mock<IKeyDerivationWrapper> keyDerivationWrapperMock = new Mock<IKeyDerivationWrapper>();
        public Mock<ISaltGenerator> saltGeneratorMock = new Mock<ISaltGenerator>();

        private HasherService hasherService;
        private string password;
        private byte[] salt;
        private byte[] hash;
        private string hashedPassword;

        public HasherServiceTest()
        {
            hasherService = new HasherService(keyDerivationWrapperMock.Object, 
                saltGeneratorMock.Object);
            password = "1234569I";
            salt = new byte[NumberConstants.SaltSize];
            hash = new byte[NumberConstants.KeySize];
                using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
                rng.GetBytes(hash);
            }
            hashedPassword = "hdhndknjks";
        }

        [Fact]
        public void HashPassword_NoErrorOrIssues_Expect_PasswornAndSalt()
        {
            //Arrange
            saltGeneratorMock.Setup(x => x.GenerateSalt())
                .Returns(salt);
            keyDerivationWrapperMock.Setup(x => x.Pbkdf2(password,
                salt, 
                KeyDerivationPrf.HMACSHA256, 
                NumberConstants.IterationCount, 
                NumberConstants.KeySize))
                .Returns(hash);

            var outputBytes = new byte[NumberConstants.KeySize];
            Buffer.BlockCopy(hash, 0, outputBytes, 0, NumberConstants.KeySize);

            //Act
            var result = hasherService.HashPassword(password);

            //Assert
            saltGeneratorMock.Verify(x => x.GenerateSalt(), Times.Once());
            keyDerivationWrapperMock.Verify(x => x.Pbkdf2(
                It.Is<string>(x => x == password),
                It.Is<byte[]>(x => x == salt),
                It.Is<KeyDerivationPrf>(x => x == KeyDerivationPrf.HMACSHA256),
                It.Is<int>(x => x == NumberConstants.IterationCount),
                It.Is<int>(x => x == NumberConstants.KeySize)), Times.Once());

            Assert.Equal(Convert.ToBase64String(outputBytes), result.Password);
            Assert.Equal(Convert.ToBase64String(salt), result.Salt);
        }

        [Fact]
        public void VerifyHashedPassword_NoErrorOrIssues_Expect_PasswordVeryfied()
        {
            //Arrange
            keyDerivationWrapperMock.Setup(x => x.Pbkdf2(password,
                salt,
                KeyDerivationPrf.HMACSHA256,
                NumberConstants.IterationCount,
                NumberConstants.KeySize))
                .Returns(hash);

            //Act
            var result = hasherService.VerifyHashedPassword(
                password, 
                Convert.ToBase64String(hash), 
                Convert.ToBase64String(salt));

            //Assert
            keyDerivationWrapperMock.Verify(x => x.Pbkdf2(
                It.Is<string>(x => x == password),
                It.IsAny<byte[]>(),
                It.Is<KeyDerivationPrf>(x => x == KeyDerivationPrf.HMACSHA256),
                It.Is<int>(x => x == NumberConstants.IterationCount),
                It.Is<int>(x => x == NumberConstants.KeySize)), Times.Once());

            Assert.True(result);
        }

        [Fact]
        public void VerifyHashedPassword_NoHashedPassword_Expect_ArgumentNullException()
        {
            //Arrange
            //Act
            var ex = Assert.Throws<ArgumentNullException>(() => hasherService.VerifyHashedPassword("dumy", null, "dumy"));

            //Assert
            Assert.Equal("hashedPassword", ex.ParamName);
        }
        [Fact]
        public void VerifyHashedPassword_NoProvidedPassword_Expect_ArgumentNullException()
        {
            //Arrange
            //Act
            var ex = Assert.Throws<ArgumentNullException>(() => hasherService.VerifyHashedPassword(null, "dumy", "dumy"));

            //Assert
            Assert.Equal("providedPassword", ex.ParamName);
        }

        [Fact]
        public void VerifyHashedPassword_EmptyHashedPassword_Expect_PasswordInvalid()
        {
            //Arrange
            //Act
            var result = hasherService.VerifyHashedPassword(password, string.Empty, "dumy");

            //Assert
            Assert.False(result);
        }
    }
}
