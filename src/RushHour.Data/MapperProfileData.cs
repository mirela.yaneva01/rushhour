﻿using AutoMapper;
using RushHour.Data.Entities;
using RushHour.Domain.Dtos;

namespace RushHour.Data
{
    public class MapperProfileData : Profile
    {
        public MapperProfileData()
        {
            CreateMap<UserDto, UserEntity>();
            CreateMap<ActivityDto, ActivityEntity>();
            CreateMap<AppointmentDto, AppointmentEntity>();
            CreateMap<AppointmentActivityDto, AppointmentActivityEntity>();

            CreateMap<UserEntity, UserDto>();
            CreateMap<ActivityEntity, ActivityDto>();
            CreateMap<AppointmentEntity, AppointmentDto>();
            CreateMap<AppointmentActivityEntity, AppointmentActivityDto>();
        }
    }
}
