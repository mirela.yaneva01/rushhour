﻿using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entities;

namespace RushHour.Data
{
    public class RushHourDbContext : DbContext
    {
        public DbSet <UserEntity> Users { get; set; }
        public DbSet <AppointmentEntity> Appointments { get; set; }
        public DbSet <ActivityEntity> Activities { get; set; }
        public DbSet <AppointmentActivityEntity> AppointmentActivities { get; set; }

        public RushHourDbContext(DbContextOptions<RushHourDbContext> options)
        : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserEntity>()
                .Property(c => c.Role)
                .HasConversion<int>();

            modelBuilder.Entity<AppointmentActivityEntity>()
                .HasOne(x => x.Appointment)
                .WithMany(x => x.AppointmentActiities)
                .HasForeignKey(x => x.AppointmentId);

            modelBuilder.Entity<AppointmentActivityEntity>()
                .HasOne(x => x.Activity)
                .WithMany(x => x.AppointmentActivities)
                .HasForeignKey(x => x.ActivityId);

            base.OnModelCreating(modelBuilder);

        }
    }
}