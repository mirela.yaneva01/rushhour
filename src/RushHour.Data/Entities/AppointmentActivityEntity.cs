﻿using System;

namespace RushHour.Data.Entities
{
    public class AppointmentActivityEntity : BaseEntity
    {
        public Guid ActivityId { get; set; }
        public Guid AppointmentId { get; set; }
        public AppointmentEntity Appointment { get; set; }
        public ActivityEntity Activity { get; set; }
    }
}
