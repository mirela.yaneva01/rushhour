﻿using System;
using System.Collections.Generic;

namespace RushHour.Data.Entities
{
    public class AppointmentEntity : BaseEntity
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid UserId { get; set; }
        public UserEntity User { get; set; }
        public List<AppointmentActivityEntity> AppointmentActiities { get; set; }
    }
}
