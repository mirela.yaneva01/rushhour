﻿using System.Collections.Generic;

namespace RushHour.Data.Entities
{
    public class ActivityEntity : BaseEntity
    {
        public string Name { get; set; }
        public int DurationMinutes { get; set; }
        public decimal Price { get; set; }
        public List<AppointmentActivityEntity> AppointmentActivities { get; set; }
    }
}
