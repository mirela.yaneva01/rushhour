﻿using RushHour.Domain.Enums;
using System.Collections.Generic;

namespace RushHour.Data.Entities
{
    public class UserEntity : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
        public List<AppointmentEntity> Appointments { get; set; }
        public string Salt { get; set; }
    }
}
