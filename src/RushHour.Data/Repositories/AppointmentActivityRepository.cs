﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entities;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class AppointmentActivityRepository : BaseRepository<AppointmentActivityDto, AppointmentActivityEntity>, IAppointmentActivityRepository
    {
        public AppointmentActivityRepository(IMapper mapper, RushHourDbContext context) : base(mapper, context)
        {
        }

        public async Task<decimal> SumForDurationAppointmentAsync(Guid appointmentId)
        {
            return await entities
                .Where(x => x.AppointmentId == appointmentId)
                .SumAsync(x => x.Activity.DurationMinutes);
        }

        public async Task<List<AppointmentActivityDto>> GetAllActivitiesForAppointmentAsync(Guid appointmentId)
        {
            var query = entities.Where(x => x.AppointmentId == appointmentId);

            return await _mapper.ProjectTo<AppointmentActivityDto>(query).ToListAsync();
        }
    }
}
