﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entities;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class ActivityRepository : BaseRepository<ActivityDto, ActivityEntity>, IActivityRepository
    {
        public ActivityRepository(IMapper mapper, RushHourDbContext context) : base(mapper, context)
        {
        }
    }
}
