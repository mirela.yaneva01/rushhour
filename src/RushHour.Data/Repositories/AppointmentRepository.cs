﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entities;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class AppointmentRepository : BaseRepository<AppointmentDto, AppointmentEntity>, IAppointmentRepository
    {
        public AppointmentRepository(IMapper mapper, RushHourDbContext context) : base(mapper, context)
        {
        }

        
    }
}
