﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entities;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public abstract class BaseRepository<T, TEntity> : IBaseRepository<T>
        where T : BaseDto
        where TEntity : BaseEntity
    {
        protected readonly RushHourDbContext _context;
        protected DbSet<TEntity> entities;
        protected IMapper _mapper;

        public BaseRepository(IMapper mapper, RushHourDbContext context)
        {
            _mapper = mapper;
            _context = context;
            entities = _context.Set<TEntity>();
        }

        public async Task CreateAsync(T dto)
        {
            entities.Add(_mapper.Map<TEntity>(dto));
            await _context.SaveChangesAsync();
        }

        public async Task<List<T>> GetAllAsync(int? offset, int? limit, Expression<Func<T, bool>> filter = null)
        {
            var query = entities.AsQueryable();

            if (filter != null)
            {
                var filterDb = _mapper.Map<Expression<Func<TEntity, bool>>>(filter);
                query = query.Where(filterDb);
            }

            if (offset != null)
            {
                query = query.Skip(offset.Value);
            }
            if (limit != null)
            {
                query = query.Take(limit.Value);
            }

            return await _mapper.ProjectTo<T>(query).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var dto = _mapper.Map<T>(await entities.FirstOrDefaultAsync(x => x.Id == id));
            return dto;
        }

        public async Task UpdateAsync(T dto)
        {
            var entity = await entities.FirstOrDefaultAsync(x => x.Id == dto.Id);

            _mapper.Map(dto, entity);
            entities.Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await entities.FirstOrDefaultAsync(x => x.Id == id);
            entities.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> filter)
        {
            return await entities.AnyAsync(_mapper.Map<Expression<Func<TEntity, bool>>>(filter));
        }
    }
}
