﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RushHour.Data.Entities;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Dtos;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class UserRepository : BaseRepository<UserDto, UserEntity>, IUserRepository
    {
        public UserRepository(IMapper mapper, RushHourDbContext context) : base(mapper, context)
        {
        }

        public async Task<UserDto> GetByEmailAsync(string email)
        {
            return await _mapper.ProjectTo<UserDto>(entities.Where(x => x.Email == email)).FirstOrDefaultAsync();
        }
    }
}
