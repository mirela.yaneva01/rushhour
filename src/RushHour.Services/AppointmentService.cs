﻿using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Abstractions.Services;
using RushHour.Domain.Constants;
using RushHour.Domain.Dtos;
using RushHour.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Services
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IAppointmentRepository _appointmentRepository;
        private readonly IActivityRepository _activityRepository;
        private readonly IAppointmentActivityRepository _appointmentActivityRepository;

        public AppointmentService(IAppointmentRepository appointmentRepository, 
            IActivityRepository activityRepository, 
            IAppointmentActivityRepository appointmentActivityRepository)
        {
            _appointmentRepository = appointmentRepository;
            _activityRepository = activityRepository;
            _appointmentActivityRepository = appointmentActivityRepository;
        }
        public async Task CreateAsync(AppointmentDto dto, Guid userId)
        {
            var allAppointments = await GetAllAsync(null, null, userId);

            if (allAppointments.Any(x => dto.StartDate > x.StartDate && dto.StartDate < x.EndDate 
                || dto.EndDate > x.StartDate &&  dto.EndDate < x.EndDate
                || dto.StartDate < x.StartDate && dto.EndDate > x.EndDate))
            {
                throw new AppException(StringConstants.InvalidAppointment);
            }

            dto.UserId = userId;
            await _appointmentRepository.CreateAsync(dto);
        }

        public async Task<List<AppointmentDto>> GetAllAsync(int? offset, int? limit, Guid userId)
        {
            return await _appointmentRepository.GetAllAsync(offset, limit, x => x.UserId == userId);
        }

        public async Task<AppointmentDto> GetByIdAsync(Guid id, Guid userId)
        {
            var appointment = await _appointmentRepository.GetByIdAsync(id);

            if (appointment.UserId != userId)
            {
                return null;
            }

            return appointment;
        }

        public async Task UpdateAsync(AppointmentDto dto, Guid userId)
        {
            if (!await _appointmentRepository.AnyAsync(x => x.Id == dto.Id && x.UserId == userId))
            {
                return;
            }

            await _appointmentRepository.UpdateAsync(dto);
        }

        public async Task DeleteAsync(Guid id, Guid userId)
        {
            if (!await _appointmentRepository.AnyAsync(x => x.Id == id && x.UserId == userId))
            {
                return;
            }

            await _appointmentRepository.DeleteAsync(id);
        }

        public async Task AddActivityToAppointmentAsync(Guid appointmentId, Guid userId, Guid activityId)
        {
            var appointment = await GetByIdAsync(appointmentId, userId);

            if (appointment == null)
            {
                return;
            }
           
            var newActivity = await _activityRepository.GetByIdAsync(activityId);
            var appointmentLength = (appointment.EndDate - appointment.StartDate).TotalMinutes;
            var existingActivityLength = await _appointmentActivityRepository.SumForDurationAppointmentAsync(appointmentId);

            if ((decimal)appointmentLength < existingActivityLength + newActivity.DurationMinutes)
            {
                throw new AppException(StringConstants.InvalidActivity);
            }

            await _appointmentActivityRepository.CreateAsync(new AppointmentActivityDto
            {
                ActivityId = activityId,
                AppointmentId = appointmentId
            });
        }

        public async Task<List<ActivityDto>> GetActivitiesForAppointmentAsync(Guid id, Guid userId)
        {
            if (!await _appointmentRepository.AnyAsync(x => x.Id == id && x.UserId == userId))
            {
                return null;
            }
            var appointmentActivities = await _appointmentActivityRepository.GetAllActivitiesForAppointmentAsync(id);

            var activityList = new List<ActivityDto>();

            foreach (var appointmentActivity in appointmentActivities)
            {
                activityList.Add(await _activityRepository.GetByIdAsync(appointmentActivity.ActivityId));
            }

            return activityList;
        }
    }
}
