﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Collections.Generic;
using System.Security.Claims;
using RushHour.Domain.Enums;
using RushHour.Domain.Abstractions.Helpers;
using Microsoft.Extensions.Options;
using RushHour.Domain;

namespace RushHour.Services
{
    public class JwtService : IJwtService
    {
        private readonly JwtSettings _settings;
        private readonly IDateTimeProvider _dateTimeProvider;

        public JwtService(IOptions<JwtSettings> settings, IDateTimeProvider dateTimeProvider)
        {
            _settings = settings.Value;
            _dateTimeProvider = dateTimeProvider;
        }

        public string GenerateJsonWebToken(Role role, Guid userId)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Role, role.ToString()));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, userId.ToString()));

            var token = new JwtSecurityToken(claims: claims,
                audience: _settings.Audience,
                issuer: _settings.Issuer,
                expires: _dateTimeProvider.GetNow().Add(TimeSpan.FromMinutes(_settings.ExpirationInMinutes)),
                signingCredentials: credentials);
            
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
