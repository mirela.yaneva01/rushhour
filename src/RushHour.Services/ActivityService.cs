﻿using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Abstractions.Services;
using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Services
{
    public class ActivityService : IActivityService
    {
        private readonly IActivityRepository _activityRepository;

        public ActivityService(IActivityRepository activityRepository)
        {
            _activityRepository = activityRepository;
        }

        public async Task CreateAsync(ActivityDto dto)
        {
            await _activityRepository.CreateAsync(dto);
        }

        public async Task<List<ActivityDto>> GetAllAsync(int? offset, int? limit)
        {
            return await _activityRepository.GetAllAsync(offset, limit);
        }

        public async Task<ActivityDto> GetByIdAsync(Guid id)
        {
            return await _activityRepository.GetByIdAsync(id);
        }

        public async Task UpdateAsync(ActivityDto dto)
        {
            await _activityRepository.UpdateAsync(dto);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _activityRepository.DeleteAsync(id);
        }
    }
}
