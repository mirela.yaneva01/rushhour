﻿using RushHour.Domain.Abstractions.Helpers;
using System;

namespace RushHour.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetNow() => DateTime.Now;
    }
}
