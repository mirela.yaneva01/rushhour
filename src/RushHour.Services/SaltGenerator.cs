﻿using RushHour.Domain.Abstractions.Helpers;
using RushHour.Domain.Constants;
using System;
using System.Security.Cryptography;

namespace RushHour.Services
{
    public class SaltGenerator : ISaltGenerator
    {
        public byte[] GenerateSalt()
        {
            byte[] salt = new byte[NumberConstants.SaltSize];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }
    }
}
