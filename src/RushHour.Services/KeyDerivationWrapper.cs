﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using RushHour.Domain.Abstractions.Helpers;
using RushHour.Domain.Constants;

namespace RushHour.Services
{
    public class KeyDerivationWrapper : IKeyDerivationWrapper
    {
        public byte[] Pbkdf2(string password, byte[] salt, KeyDerivationPrf prf, int iterationCount, int numBytesRequested)
        {
            return KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: NumberConstants.IterationCount,
                numBytesRequested: NumberConstants.KeySize);
        }
    }
}
