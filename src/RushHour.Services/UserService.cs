﻿using Microsoft.Extensions.Options;
using RushHour.Domain;
using RushHour.Domain.Abstractions.Helpers;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Abstractions.Services;
using RushHour.Domain.Constants;
using RushHour.Domain.Dtos;
using RushHour.Domain.Enums;
using RushHour.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userepository;
        private readonly IHasherService _hasher;
        private readonly IJwtService _jwtToken;

        public UserService(IUserRepository userRepository, IHasherService hasher, IJwtService jwtToken)
        {
            _userepository = userRepository;
            _hasher = hasher;
            _jwtToken = jwtToken;
        }

        public async Task CreateAsync(UserDto dto)
        {
            if (await _userepository.GetByEmailAsync(dto.Email) != null)
            {
                throw new AppException(StringConstants.UserExists);
            }

            var passAndSalt = _hasher.HashPassword(dto.Password);
            dto.Role = Role.User;
            dto.Password = passAndSalt.Password;
            dto.Salt = passAndSalt.Salt;
            dto.Email = dto.Email.ToLower();
            await _userepository.CreateAsync(dto);
        }

        public async Task<List<UserDto>> GetAllAsync(int? offset, int? limit)
        {
            return await _userepository.GetAllAsync(offset, limit);
        }

        public async Task<UserDto> GetByIdAsync(Guid id)
        {
            return await _userepository.GetByIdAsync(id);
        }

        public async Task UpdateAsync(UserDto dto)
        {
            await _userepository.UpdateAsync(dto);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _userepository.DeleteAsync(id);
        }

        public async Task<string> LoginAsync(string email, string password)
        {
            UserDto user = await _userepository.GetByEmailAsync(email);
            if (user == null || !_hasher.VerifyHashedPassword(password, user.Password, user.Salt))
            {
                throw new AppException(StringConstants.InvalidUserOrPass);
            }
            return _jwtToken.GenerateJsonWebToken(user.Role, user.Id);
        }
    }
}
