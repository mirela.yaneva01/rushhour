using Autofac;
using Autofac.Extensions.DependencyInjection;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;
using RushHour.Api.Extensions;
using RushHour.Api.Middleware;
using RushHour.Api.Validators;
using RushHour.Data;
using RushHour.Data.Repositories;
using RushHour.Domain;
using RushHour.Domain.Abstractions.Helpers;
using RushHour.Services;
using System.Reflection;

namespace RushHour.Api
{
    public class Startup
    {
        readonly string CorsWithOriginsPolicyName = "_corsWithOriginsPolicyName";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var corsSettings = Configuration.GetSection(typeof(CorsSettings).Name).Get<CorsSettings>();

            services.AddCors(options =>
            {
                options.AddPolicy(name: CorsWithOriginsPolicyName,
                                  builder =>
                                  {
                                      builder.WithOrigins(corsSettings.Origins)
                                      .AllowAnyHeader();
                                  });
            });

            services.AddAutofac();
            services.AddControllers()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<ActivityValidator>());

            services.AddDbContext<RushHourDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("Sql")));

            services.AddSwagger();
            services.AddAuth(Configuration);
            services.AddAutoMapper();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(ActivityRepository)))
                   .Where(t => t.Name.EndsWith("Repository"))
                   .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(ActivityService)))
                   .Where(t => t.Name.EndsWith("Service"))
                   .AsImplementedInterfaces();

            builder.RegisterType<KeyDerivationWrapper>().As<IKeyDerivationWrapper>().InstancePerLifetimeScope();
            builder.RegisterType<SaltGenerator>().As<ISaltGenerator>().InstancePerLifetimeScope();
            builder.RegisterType<DateTimeProvider>().As<IDateTimeProvider>().InstancePerLifetimeScope();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseCors(CorsWithOriginsPolicyName);

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Rush Hour V1");
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}
