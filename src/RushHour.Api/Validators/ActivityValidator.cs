﻿using FluentValidation;
using RushHour.Api.Models;

namespace RushHour.Api.Validators
{
    public class ActivityValidator : AbstractValidator<ActivityModel>
    {
        public ActivityValidator()
        {
            RuleFor(x => x.Name).Length(5, 30);
            RuleFor(x => x.Price).InclusiveBetween(0.01m, decimal.MaxValue);
            RuleFor(x => x.DurationMinutes).InclusiveBetween(1, int.MaxValue);
        }
    }
}
