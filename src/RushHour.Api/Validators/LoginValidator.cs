﻿using FluentValidation;
using RushHour.Api.Models;

namespace RushHour.Api.Validators
{
    public class LoginValidator : AbstractValidator<LoginModel>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Password).Length(5, 15);
        }
    }
}
