﻿using FluentValidation;
using RushHour.Api.Models;

namespace RushHour.Api.Validators
{
    public class UserValidator : AbstractValidator<UserModel>
    {
        public UserValidator()
        {
            RuleFor(x => x.FirstName).Length(3, 10);
            RuleFor(x => x.LastName).Length(3, 10);
            RuleFor(x => x.Password).Length(5, 15);
            RuleFor(x => x.Email).EmailAddress();
        }
    }
}
