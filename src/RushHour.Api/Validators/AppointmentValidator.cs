﻿using FluentValidation;
using RushHour.Api.Models;
using System;

namespace RushHour.Api.Validators
{
    public class AppointmentValidator : AbstractValidator<AppointmentModel>
    {
        public AppointmentValidator()
        {
            RuleFor(x => x.UserID).NotNull();
            RuleFor(x => x.StartDate).InclusiveBetween(DateTime.UtcNow, DateTime.MaxValue);
            RuleFor(x => x.EndDate).InclusiveBetween(DateTime.UtcNow, DateTime.MaxValue);
            
        }
    }
}
