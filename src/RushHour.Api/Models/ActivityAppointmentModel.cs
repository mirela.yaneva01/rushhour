﻿using System;

namespace RushHour.Api.Models
{
    public class ActivityAppointmentModel
    {
        public Guid ActivityId { get; set; }
        public Guid AppointmentId { get; set; }
    }
}
