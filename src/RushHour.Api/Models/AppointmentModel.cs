﻿using System;

namespace RushHour.Api.Models
{
    public class AppointmentModel : BaseModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid UserID { get; set; }
    }
}
