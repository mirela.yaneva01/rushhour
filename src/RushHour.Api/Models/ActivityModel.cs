﻿namespace RushHour.Api.Models
{
    public class ActivityModel : BaseModel
    {
        public string Name { get; set; }
       
        public int DurationMinutes { get; set; }

        public decimal Price { get; set; }
    }
}
