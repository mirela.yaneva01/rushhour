﻿using AutoMapper;
using RushHour.Api.Models;
using RushHour.Domain.Dtos;

namespace RushHour.Api
{
    public class MapperProfileApi : Profile
    {
        public MapperProfileApi()
        {
            CreateMap<UserModel, UserDto>()
                .ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<AppointmentModel, AppointmentDto>()
                .ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<ActivityModel, ActivityDto>()
                .ForMember(x => x.Id, opt => opt.Ignore());

            CreateMap<UserDto, UserModel>();
            CreateMap<AppointmentDto, AppointmentModel>();
            CreateMap<ActivityDto, ActivityModel>();
        }
    }
}
