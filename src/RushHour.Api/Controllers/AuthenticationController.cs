﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RushHour.Api.Models;
using RushHour.Domain.Abstractions.Services;
using RushHour.Domain.Dtos;
using System.Threading.Tasks;

namespace RushHour.Api.Controllers
{
    [ApiController]
    [Route("auth")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public AuthenticationController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dto = _mapper.Map<UserDto>(model);

            await _userService.CreateAsync(dto);

            return CreatedAtRoute("Login", model);
        }

        [HttpPost("login", Name = "Login")]
        public async Task<IActionResult> LoginAcync([FromBody] LoginModel login)
        {
           return Ok(await _userService.LoginAsync(login.Email, login.Password));
        }
    }
}
