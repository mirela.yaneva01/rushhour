﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RushHour.Api.Models;
using RushHour.Domain.Abstractions.Services;
using RushHour.Domain.Constants;
using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Api.Controllers
{
    [ApiController]
    [Route("activities")]
    public class ActivityContoller : ControllerBase
    {
        private readonly IActivityService _activityService;
        private readonly IMapper _mapper;

        public ActivityContoller(IActivityService activityService, IMapper mapper)
        {
            _activityService = activityService;
            _mapper = mapper;
        }

        [HttpGet(Name = "GetActivities")]
        [Authorize(Policy = StringConstants.AdminUserPolicy)]
        public async Task<IActionResult> GetAllAsync(int? offset, int? limit)
        {
            var dtos = await _activityService.GetAllAsync(offset, limit);
            return Ok(_mapper.Map<List<ActivityModel>>(dtos));
        }

        [HttpGet("{id}")]
        [Authorize(Policy = StringConstants.AdminUserPolicy)]
        public async Task<IActionResult> GetAsync([FromRoute] Guid id)
        {
            var dto = await _activityService.GetByIdAsync(id);
            return Ok(_mapper.Map<ActivityModel>(dto));
        }

        [HttpPost]
        [Authorize(Policy = StringConstants.AdminPolicy)]
        public async Task<IActionResult> PostAsync([FromBody] ActivityModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dto = _mapper.Map<ActivityDto>(model);
            await _activityService.CreateAsync(dto);
            return CreatedAtRoute("GetActivities", model);
        }

        [HttpPut]
        [Authorize(Policy = StringConstants.AdminPolicy)]
        public async Task<IActionResult> PutAsync([FromBody] ActivityModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dto = _mapper.Map<ActivityDto>(model);
            await _activityService.UpdateAsync(dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = StringConstants.AdminPolicy)]
        public async Task<IActionResult> DeleteAsync([FromQuery] Guid id)
        {
            await _activityService.DeleteAsync(id);
            return NoContent();
        }
    }
}
