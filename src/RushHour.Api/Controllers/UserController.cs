﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RushHour.Api.Models;
using RushHour.Domain.Abstractions.Services;
using RushHour.Domain.Constants;
using RushHour.Domain.Dtos;
using RushHour.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Api.Controllers
{
    [ApiController]
    [Route("users")]
    [Authorize(Policy = StringConstants.AdminPolicy)]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync([FromQuery] int? offset = null, [FromQuery] int? limit = null)
        {
           var dtos = await _userService.GetAllAsync(offset, limit);
           return Ok(_mapper.Map<List<UserModel>>(dtos));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute] Guid id)
        {
            var dto = await _userService.GetByIdAsync(id);
            return Ok(_mapper.Map<UserModel>(dto));
        }

        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] UserModel model)
        {
            var dto = _mapper.Map<UserDto>(model);
            await _userService.UpdateAsync(dto);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromQuery] Guid id)
        {
            await _userService.DeleteAsync(id);
            return Ok();
        }
    }
}
