﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RushHour.Api.Models;
using RushHour.Domain.Abstractions.Services;
using RushHour.Domain.Constants;
using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RushHour.Api.Controllers
{
    [ApiController]
    [Route("appointments")]
    [Authorize(Policy = StringConstants.AdminUserPolicy)]
    public class AppointmentContoller : ControllerBase
    {
        private readonly IAppointmentService _appointmentService;
        private readonly IMapper _mapper;

        public AppointmentContoller(IAppointmentService appointmentService, IMapper mapper)
        {
            _appointmentService = appointmentService;
            _mapper = mapper;
        }

        [HttpGet(Name = "GetAppointments")]
        public async Task<IActionResult> GetAllAsync(int? offset, int? limit)
        {
            var dtos = await _appointmentService
                .GetAllAsync(offset, limit, Guid.Parse(GetUserId()));
            return Ok(_mapper.Map<List<AppointmentModel>>(dtos));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute] Guid id)
        {
            var dto = await _appointmentService.GetByIdAsync(id, Guid.Parse(GetUserId()));
            return Ok(_mapper.Map<AppointmentModel>(dto));
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] AppointmentModel model)
        {
            var dto = _mapper.Map<AppointmentDto>(model);

            await _appointmentService.CreateAsync(dto, Guid.Parse(GetUserId()));
            return CreatedAtRoute("GetAppointments", model);
        }

        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] AppointmentModel model)
        {
            var dto = _mapper.Map<AppointmentDto>(model);
            await _appointmentService.UpdateAsync(dto, Guid.Parse(GetUserId()));
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromQuery] Guid id)
        {
            await _appointmentService.DeleteAsync(id, Guid.Parse(GetUserId()));
            return NoContent();
        }

        [HttpGet("{id}/activities", Name = "GetActivitiesForAppointment")]
        public async Task<IActionResult> GetActivitiesForAppointmentAsync([FromRoute] Guid id)
        {
            return Ok(await _appointmentService.GetActivitiesForAppointmentAsync(id,
                Guid.Parse(GetUserId())));
        }

        [HttpPost("add-activity")]
        public async Task<IActionResult> AddActivityToAppointmentAsync([FromBody] ActivityAppointmentModel model)
        {
            await _appointmentService.AddActivityToAppointmentAsync(model.AppointmentId,
                Guid.Parse(GetUserId()),
                model.ActivityId);

            return CreatedAtAction("GetActivitiesForAppointment", new { id = model.AppointmentId}, model);
        }

        private string GetUserId()
        {
            return User.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value;
        }
    }
}
