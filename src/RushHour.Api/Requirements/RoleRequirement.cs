﻿using Microsoft.AspNetCore.Authorization;
using RushHour.Domain.Enums;

namespace RushHour.Api.Requirements
{
    public class RoleRequirement : IAuthorizationRequirement
    {
        public Role Role { get; set; }

        public RoleRequirement(Role role)
        {
            Role = role;
        }
    }
}
