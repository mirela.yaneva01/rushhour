﻿using AutoMapper.Extensions.ExpressionMapping;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RushHour.Api.Middleware;
using RushHour.Api.Requirements;
using RushHour.Data;
using RushHour.Data.Repositories;
using RushHour.Domain;
using RushHour.Domain.Abstractions.Helpers;
using RushHour.Domain.Abstractions.Repositories;
using RushHour.Domain.Abstractions.Services;
using RushHour.Domain.Constants;
using RushHour.Domain.Enums;
using RushHour.Services;
using System.Security.Claims;
using System.Text;

namespace RushHour.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Rush Hour", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Name = "Authorization"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[]{}
                    }
                });
            });
        }

        public static void AddAuth(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtSettingsSection = configuration.GetSection(typeof(JwtSettings).Name);
            JwtSettings settings = jwtSettingsSection.Get<JwtSettings>();
            services.Configure<JwtSettings>(jwtSettingsSection);

            services.AddAuthentication(cfg => cfg.DefaultScheme = JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    // This should be delete when in production
                    options.RequireHttpsMetadata = settings.RequireHttpsMetadata;
                    options.SaveToken = settings.SaveToken;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = settings.ValidateIssuer,
                        ValidateAudience = settings.ValidateAudience,
                        ValidateLifetime = settings.ValidateLifetime,
                        ValidateIssuerSigningKey = settings.ValidateIssuerSigningKey,
                        ValidIssuer = settings.Issuer,
                        ValidAudience = settings.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.Key))
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(StringConstants.AdminPolicy, policy =>
                {
                    policy.Requirements.Add(new RoleRequirement(Role.Admin));
                    policy.RequireClaim(ClaimTypes.Role);
                });
                
                options.AddPolicy(StringConstants.UserPolicy, policy =>
                {
                    policy.Requirements.Add(new RoleRequirement(Role.User));
                    policy.RequireClaim(ClaimTypes.Role);
                });
                
                options.AddPolicy(StringConstants.AdminUserPolicy, policy =>
                {
                    policy.RequireClaim(ClaimTypes.Role);
                    policy.Requirements.Add(new RoleRequirement(Role.Admin | Role.User));
                });
            });

            services.AddSingleton<IAuthorizationHandler, RoleHandler>();

        }

        public static void AddAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(mc =>
            {
                mc.AddExpressionMapping();
                mc.AddProfile(new MapperProfileData());
                mc.AddProfile(new MapperProfileApi());
            });
        }
    }
}
