﻿namespace RushHour.Domain
{
    public class CorsSettings
    {
        public string[] Origins { get; set; }
    }
}
