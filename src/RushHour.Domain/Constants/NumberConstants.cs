﻿namespace RushHour.Domain.Constants
{
    public class NumberConstants
    {
        public const int SaltSize = 16; // 128 bit / 8
        public const int KeySize = 32; // 256 bit /8
        public const int IterationCount = 10000;
    }
}
