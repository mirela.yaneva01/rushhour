﻿namespace RushHour.Domain.Constants
{
    public class StringConstants
    {
        public const string AdminPolicy = "Admin";
        public const string UserPolicy = "User";
        public const string AdminUserPolicy = "AdminUser";

        public const string InvalidUserOrPass = "Invalid username or password!";
        public const string UserExists = "User with given email already exitst!";
        public const string InvalidToken = "Invalid token!";
        public const string InvalidAppointment = "Appointment time interval overlaps with existing appointment!";
        public const string InvalidActivity = "Activity cant't fit in appointment!";
    }
}
