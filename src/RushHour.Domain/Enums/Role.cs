﻿using System;

namespace RushHour.Domain.Enums
{
    [Flags]
    public enum Role
    {
        Admin = 1,
        User = 2
    }
}
