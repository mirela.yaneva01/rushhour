﻿using RushHour.Domain.Enums;
using System;
using System.Collections.Generic;

namespace RushHour.Domain.Dtos
{
    public class UserDto : BaseDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
        public string Salt { get; set; }
    }
}