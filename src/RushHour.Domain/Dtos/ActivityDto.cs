﻿using System;
using System.Collections.Generic;

namespace RushHour.Domain.Dtos
{
    public class ActivityDto : BaseDto
    {
        public string Name { get; set; }
        public int DurationMinutes { get; set; }
        public decimal Price { get; set; }
    }
}
