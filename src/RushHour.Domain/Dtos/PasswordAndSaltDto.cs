﻿namespace RushHour.Domain.Dtos
{
    public class PasswordAndSaltDto
    {
        public string Password { get; set; }
        public string Salt { get; set; }
    }
}
