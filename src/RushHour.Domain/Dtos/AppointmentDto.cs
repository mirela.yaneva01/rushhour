﻿using System;
using System.Collections.Generic;

namespace RushHour.Domain.Dtos
{
    public class AppointmentDto : BaseDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid UserId { get; set; }
    }
}
