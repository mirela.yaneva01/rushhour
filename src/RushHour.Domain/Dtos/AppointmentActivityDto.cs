﻿using System;

namespace RushHour.Domain.Dtos
{
    public class AppointmentActivityDto : BaseDto
    {
        public Guid ActivityId { get; set; }
        public Guid AppointmentId { get; set; }
    }
}
