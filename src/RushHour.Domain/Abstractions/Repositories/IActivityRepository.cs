﻿using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RushHour.Domain.Abstractions.Repositories
{
    public interface IActivityRepository : IBaseRepository<ActivityDto>
    {
    }
}
