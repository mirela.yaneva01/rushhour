﻿using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RushHour.Domain.Abstractions.Repositories
{
    public interface IBaseRepository<T>
        where T : BaseDto
    {
        Task<List<T>> GetAllAsync(int? offset, int? limit, Expression<Func<T, bool>> filter = null);
        Task<T> GetByIdAsync(Guid id);
        Task CreateAsync(T dto);
        Task UpdateAsync(T dto);
        Task DeleteAsync(Guid id);
        Task<bool> AnyAsync(Expression<Func<T, bool>> filter);
    }
}
