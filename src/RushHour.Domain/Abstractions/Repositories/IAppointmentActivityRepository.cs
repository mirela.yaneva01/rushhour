﻿using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Abstractions.Repositories
{
    public interface IAppointmentActivityRepository : IBaseRepository<AppointmentActivityDto>
    {
        Task<decimal> SumForDurationAppointmentAsync(Guid appointmentId);
        Task<List<AppointmentActivityDto>> GetAllActivitiesForAppointmentAsync(Guid appointmentId);
    }
}
