﻿using RushHour.Domain.Dtos;
using System;
using System.Threading.Tasks;

namespace RushHour.Domain.Abstractions.Repositories
{
    public interface IAppointmentRepository : IBaseRepository<AppointmentDto>
    {
       
    }
}
