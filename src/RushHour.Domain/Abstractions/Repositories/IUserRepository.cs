﻿using RushHour.Domain.Dtos;
using System.Threading.Tasks;

namespace RushHour.Domain.Abstractions.Repositories
{
    public interface IUserRepository : IBaseRepository<UserDto>
    {
        Task<UserDto> GetByEmailAsync(string email);
    }
}
