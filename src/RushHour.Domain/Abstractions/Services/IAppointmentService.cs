﻿using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Abstractions.Services
{
    public interface IAppointmentService
    {
        Task CreateAsync(AppointmentDto dto, Guid userId);
        Task<List<AppointmentDto>> GetAllAsync(int? offset, int? limit, Guid userId);
        Task<AppointmentDto> GetByIdAsync(Guid id, Guid userId);
        Task UpdateAsync(AppointmentDto dto, Guid userId);
        Task DeleteAsync(Guid id, Guid userId);
        Task AddActivityToAppointmentAsync(Guid appointmentId, Guid userId, Guid activityId);
        Task<List<ActivityDto>> GetActivitiesForAppointmentAsync(Guid id, Guid userId);
    }
}
