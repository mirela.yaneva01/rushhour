﻿using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Abstractions.Services
{
    public interface IActivityService
    {
        Task CreateAsync(ActivityDto dto);
        Task<List<ActivityDto>> GetAllAsync(int? offset, int? limit);
        Task<ActivityDto> GetByIdAsync(Guid id);
        Task UpdateAsync(ActivityDto dto);
        Task DeleteAsync(Guid id);
    }
}
