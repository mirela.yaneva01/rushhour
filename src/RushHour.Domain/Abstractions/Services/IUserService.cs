﻿using RushHour.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RushHour.Domain.Abstractions.Services
{
    public interface IUserService
    {
        Task CreateAsync(UserDto dto);
        Task<List<UserDto>> GetAllAsync(int? offset, int? limit);
        Task<UserDto> GetByIdAsync(Guid id);
        Task UpdateAsync(UserDto dto);
        Task DeleteAsync(Guid id);
        Task<string> LoginAsync(string email, string password);
    }
}
