﻿using RushHour.Domain.Enums;
using System;

namespace RushHour.Domain.Abstractions.Helpers
{
    public interface IJwtService
    {
        public string GenerateJsonWebToken(Role role, Guid userId);
    }
}
