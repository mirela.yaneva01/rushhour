﻿using System;

namespace RushHour.Domain.Abstractions.Helpers
{
    public interface IDateTimeProvider
    {
        DateTime GetNow();
    }
}
