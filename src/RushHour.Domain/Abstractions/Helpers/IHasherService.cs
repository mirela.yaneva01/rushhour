﻿using RushHour.Domain.Dtos;

namespace RushHour.Domain.Abstractions.Helpers
{
    public interface IHasherService
    {
        PasswordAndSaltDto HashPassword(string password);
        bool VerifyHashedPassword(string providedPassword, string hashedPassword, string saltString);
    }
}
