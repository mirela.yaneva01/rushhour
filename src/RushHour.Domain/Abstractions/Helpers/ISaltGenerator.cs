﻿using System;

namespace RushHour.Domain.Abstractions.Helpers
{
    public interface ISaltGenerator
    {
        byte[] GenerateSalt();
    }
}
